<?php namespace Ayedev\Integration\ApiAI\Factory;

use Ayedev\Integration\Facebook;
use Ayedev\Bot\AI\Impl\GeneratorFactory;
use Ayedev\Bot\Messenger\IFace\EventInterface;

class FacebookAPIAIFactory extends GeneratorFactory
{
    /**
     * @inheritdoc
     */
    protected function parseMessage( EventInterface $event, array $messageData )
    {
        //  Message
        $message = null;

        //  Try
        try
        {
            //  Switch
            switch( $messageData['type'] )
            {
                //  Simple Message
                case 0:

                    //  Create Message
                    $message = new Facebook\Model\Message( $this->fill( $event, $messageData['speech'] ) );
                    break;

                //  Card Message
                case 1:

                    //  Create Element
                    $element = new Facebook\Model\Generic\Element(
                        $this->fill( $event, $messageData['title'] ),
                        ( isset( $messageData['subtitle'] ) ? $this->fill( $event, $messageData['subtitle'] ) : null ),
                        ( isset( $messageData['imageUrl'] ) ? $this->fill( $event, $messageData['imageUrl'] ) : null )
                    );

                    //  Check
                    if( isset( $messageData['buttons'] ) )
                    {
                        //  Loop Each Buttons
                        foreach( $messageData['buttons'] as $buttonDef )
                        {
                            //  Type
                            $type = ( isset( $buttonDef['postback'] ) ? 'postback' : 'url' );

                            //  Type Data
                            $typeData = $buttonDef[$type];

                            //  Check Overrides
                            if( substr( $typeData, 0, 4 ) == 'url:' )
                            {
                                //  Change Type
                                $type = 'url';

                                //  Trim Data
                                $typeData = substr( $typeData, 4 );
                            }
                            else if( substr( $typeData, 0, 9 ) == 'postback:' )
                            {
                                //  Change Type
                                $type = 'postback';

                                //  Trim Data
                                $typeData = substr( $typeData, 9 );
                            }
                            else if( substr( $typeData, 0, 6 ) == 'share:' )
                            {
                                //  Change Type
                                $type = 'share';

                                //  Trim Data
                                $typeData = substr( $typeData, 6 );
                            }

                            //  Text
                            $buttonText = $this->fill( $event, $buttonDef['text'] );

                            //  Run Filler
                            $typeData = $this->fill( $event, $typeData );

                            //  Button
                            $button = null;

                            //  Switch
                            switch( $type )
                            {
                                //  Url
                                case 'url':

                                    //  Create Button
                                    $button = new Facebook\Model\Button\WebUrl( $buttonText, $typeData );
                                    break;

                                //  Postback
                                case 'postback':

                                    //  Create Button
                                    $button = new Facebook\Model\Button\Postback( $buttonText, strtoupper( $typeData ) );
                                    break;

                                //  Share
                                case 'share':

                                    //  Create Button
                                    $button = new Facebook\Model\Button\Share();
                                    break;
                            }

                            //  Check
                            if( $button )   $element->addButton( $button );
                        }

                        //  Get First Button
                        $firstButton = $element->getFirstButton();

                        //  Check
                        if( $firstButton && $firstButton instanceof Facebook\Model\Button\WebUrl )  $element->setDefaultAction( new Facebook\Model\DefaultAction( $firstButton->getUrl() ) );
                    }

                    //  Create Message
                    $message = new Facebook\Model\Generic( array( $element ) );
                    break;

                //  Quick Reply
                case 2:

                    //  Create Message
                    $message = new Facebook\Model\Message( $this->fill( $event, $messageData['title'] ) );

                    //  Loop Each Quick Replies
                    foreach( $messageData['replies'] as $replyText )
                    {
                        //  Button Payload
                        $replyPayload = null;

                        //  Check
                        if( preg_match( '/^\[(?<payload>[a-zA-Z0-9\_]+)\]/i', $replyText, $match ) )
                        {
                            //  Set Payload Text
                            $replyPayload = strtoupper( $match['payload'] );

                            //  Update the Reply Text
                            $replyText = trim( str_ireplace( $match[0], '', $replyText ) );
                        }

                        //  Check
                        if( !$replyPayload )
                        {
                            //  Create from Text
                            $replyPayload = strtoupper( 'quickreply_' . str_ireplace( ' ', '_', $replyText ) );
                        }

                        //  Append
                        $message->addQuickReply( new Facebook\Model\QuickReply( $replyText, $replyPayload ) );
                    }
                    break;

                //  Image Message
                case 3:

                    //  Create Message
                    $message = new Facebook\Model\Attachment\Image( $this->fill( $event, $messageData['imageUrl'] ) );
                    break;
            }
        } catch( \Exception $e ) {}

        //  Return
        return $message;
    }
}