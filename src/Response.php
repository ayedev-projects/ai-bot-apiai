<?php namespace Ayedev\Integration\ApiAI;

use Ayedev\Bot\AI\Core\AIResponse;
use Ayedev\Bot\AI\Core\CommonResponse;
use Ayedev\Bot\AI\IFace\AIFulfillmentInterface;
use Ayedev\Bot\Messenger\IFace\EventInterface;

class Response extends AIResponse
{
    /** @var string $_speech */
    private $_speech = null;

    /** @var string $_action */
    private $_action = null;

    /** @var bool $_actionCompleted */
    private $_actionCompleted = false;

    /** @var array $_params */
    private $_params = array();

    /** @var array $_contexts */
    private $_contexts = array();

    /** @var array $_messages */
    private $_messages = array();


    /**
     * @inheritdoc
     */
    public function parseResponse()
    {
        //  Set Success Status
        $this->_success = ( !isset( $this->_response['status'] ) ? true : $this->_response['status']['code'] == 200 );

        //  Check
        if( $this->isSuccess() && isset( $this->_response['result'] ) )
        {
            //  Store Confidence
            $this->_confidence = $this->_response['result']['score'];

            //  Store Params
            $this->_params = $this->_response['result']['parameters'];

            //  Store Action
            $this->_action = $this->_response['result']['action'];

            //  Store Action Completed
            $this->_actionCompleted = ( isset( $this->_response['result']['actionIncomplete'] ) ? !$this->_response['result']['actionIncomplete'] : true );

            //  Check
            if( isset( $this->_response['result']['fulfillment'] ) )
            {
                //  Store Speech
                $this->_speech = ( isset( $this->_response['result']['fulfillment']['speech'] ) ? $this->_response['result']['fulfillment']['speech'] : $this->_response['result']['speech'] );

                //  Store Messages
                $this->_messages = $this->_response['result']['fulfillment']['messages'];
            }
            else
            {
                //  Store Speech
                $this->_speech = $this->_response['result']['speech'];

                //  Store Messages
                $this->_messages = array(
                    array(
                        'type' => 0,
                        'speech' => $this->_speech
                    )
                );
            }

            //  Check
            if( sizeof( $this->_messages ) == 1 && $this->_messages[0]['type'] == '0' && empty( $this->_messages[0]['speech'] ) )   $this->_messages = array();

            //  Check
            if( isset( $this->_response['result']['contexts'] ) && $this->_response['result']['contexts'] )
            {
                //  Clear
                $this->_contexts = array();

                //  Loop Each
                foreach( $this->_response['result']['contexts'] as $contextData )
                {
                    //  Check
                    if( !isset( $contextData['parameters'] ) )  $contextData['parameters'] = array();

                    //  Store Context
                    $this->_contexts[$contextData['name']] = $contextData;
                }
            }
        }
    }

    /**
     * Make AIResponse from Intent Data
     *
     * @param CommonResponse $intentResponse
     * @param null $firstResponse
     * @return Response
     */
    public static function makeFromIntent( CommonResponse $intentResponse, $firstResponse = null )
    {
        //  Get Response Object
        $responseObject = $intentResponse->getResponse()['responses'][0];

        //  Params
        $params = array();

        //  Loop Each
        foreach( $responseObject['parameters'] as $paramInfo )
        {
            //  Append
            $params[$paramInfo['name']] = $paramInfo['value'];
        }

        //  Make Response Tree
        $response = array(
            'status' => array(
                'code' => 200
            ),
            'result' => array(
                'parameters' => $params,
                'fulfillment' => array(
                    'speech' => null,
                    'messages' => $responseObject['messages']
                )
            )
        );

        //  Check
        if( $firstResponse )
        {
            //  Override Parameters
            $response['result']['parameters'] = $firstResponse['result']['parameters'];

            //  Set Score
            $response['result']['score'] = $firstResponse['result']['score'];

            //  Set Action
            $response['result']['action'] = $firstResponse['result']['action'];

            //  Set Speech
            $response['result']['fulfillment']['speech'] = $firstResponse['result']['speech'];
        }

        //  Set is Fake Response
        $response['fake_response'] = true;

        //  Return
        return new static( $response );
    }


    /**
     * @inheritdoc
     */
    public function isEmpty()
    {
        //  Return
        return ( sizeof( $this->_messages ) < 1 );
    }

    /**
     * @inheritdoc
     */
    public function getSpeech()
    {
        //  Return
        return $this->_speech;
    }

    /**
     * @inheritdoc
     */
    public function actionCompleted()
    {
        //  Return
        return $this->_actionCompleted;
    }

    /**
     * @inheritdoc
     */
    public function getAction()
    {
        //  Return
        return $this->_action;
    }

    /**
     * @inheritdoc
     */
    public function getMessages()
    {
        //  Return
        return $this->_messages;
    }

    /**
     * @inheritdoc
     */
    public function hasParam( $key )
    {
        //  Return
        return isset( $this->_params[$key] );
    }

    /**
     * @inheritdoc
     */
    public function getParam( $key, $ignore_confidence = false )
    {
        //  Value
        $value = null;

        //  Check
        if( $this->hasParam( $key ) )
        {
            //  Get Value
            $value = $this->_params[$key];

            //  Check
            if( !$ignore_confidence && $this->getConfidenceLevel() < static::CONFIDENCE_LEVEL_THRESHOLD )    $value = null;
        }

        //  Return
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function getParams( $only = array(), $except = array(), $original = true )
    {
        //  Return
        return $this->filterOnlyExcept( $this->_params, $only, $except, $original );
    }

    /**
     * @inheritdoc
     */
    public function hasContext( $context )
    {
        //  Return
        return ( isset( $this->_contexts[$context] ) );
    }

    /**
     * @inheritdoc
     */
    public function getContextParam( $context, $key, $def = null )
    {
        //  Return
        return ( $this->hasContext( $context ) ? ( isset( $this->_contexts[$context]['parameters'][$key] ) ? $this->_contexts[$context]['parameters'][$key] : $def ) : $def );
    }

    /**
     * @inheritdoc
     */
    public function getContextParams( $context, $only = array(), $except = array(), $original = true )
    {
        //  Return
        return $this->filterOnlyExcept( $this->hasContext( $context ) ? $this->_contexts[$context]['parameters'] : array(), $only, $except, $original );
    }

    /**
     * @inheritdoc
     */
    public function makeContext( $context, $only = array(), $except = array() )
    {
        //  Get Context
        $context = $this->getContext( $context );

        //  Check
        if( $context )
        {
            //  Update Parameters
            $context['parameters'] = $this->filterOnlyExcept( $context['parameters'], $only, $except, true );
        }

        //  Return
        return $context;
    }

    /**
     * @inheritdoc
     */
    public function invalidateContext( $session_id, $context, $only = array(), $except = array(), $params = array() )
    {
        //  Make Context
        $context = $this->makeContext( $context, $only, $except );

        //  Check
        if( $context )
        {
            //  Return
            return mManager()->getAI()->postContext( $session_id, $context, $params );
        }

        //  Return
        return null;
    }


    /**
     * Get Filtered List
     *
     * @param $source
     * @param array $only
     * @param array $except
     * @param bool $original
     * @return array
     */
    private function filterOnlyExcept( $source, $only = array(), $except = array(), $original = true )
    {
        //  Fix Only
        if( is_string( $only ) )    $only = array( $only );
        else if ( !$only )  $only = array();
        else    $only = (array)$only;

        //  Fix Except
        if( is_string( $except ) )    $except = array( $except );
        else if ( !$except )  $except = array();
        else    $except = (array)$except;

        //  New Source
        $new_source = $source;

        //  Check
        if( $only || $except )
        {
            //  Clear
            $new_source = array();

            //  Loop Each
            foreach( $source as $key => $val )
            {
                //  Check
                if( stripos( $key, '.original' ) > -1 ) continue;

                //  Check
                if( ( $only && !in_array( $key, $only ) ) || ( $except && in_array( $key, $except ) ) ) continue;

                //  Append
                $new_source[$key] = $val;

                //  Check
                if( $original && isset( $source[$key . '.original'] ) ) $new_source[$key . '.original'] = $source[$key . '.original'];
            }
        }
        else if( !$original )
        {
            //  Clear
            $new_source = array();

            //  Loop Each
            foreach( $source as $key => $val )
            {
                //  Check
                if( stripos( $key, '.original' ) > -1 ) continue;

                //  Append
                $new_source[$key] = $val;
            }
        }

        //  Return
        return $new_source;
    }

    /**
     * @inheritdoc
     */
    public function getContext( $context )
    {
        //  Return
        return ( $this->hasContext( $context ) ? $this->_contexts[$context] : array() );
    }

    /**
     * @inheritdoc
     */
    public function getContexts()
    {
        //  Return
        return $this->_contexts;
    }


    /**
     * @inheritdoc
     */
    public function getIntentID()
    {
        //  Return
        return ( $this->isSuccess() ? $this->_response['result']['metadata']['intentId'] : null );
    }

    /**
     * @inheritdoc
     */
    public function getIntentInfo()
    {
        //  Static
        static $intent;

        //  Check
        if( !$intent )  $intent = mManager()->getWebhook()->getIntent( $this->getIntentID() );

        //  Return Intent
        return $intent;
    }

    /**
     * @inheritdoc
     */
    public function toBeAffectedContexts()
    {
        //  Contexts
        $contexts = array();

        //  Get Intent
        $intent = $this->getIntentInfo();

        //  Check
        if( $intent && $intent->isSuccess() )
        {
            //  Responses
            $responses = $intent->getResponse()['responses'];

            //  Loop Each Responses
            foreach( $responses as $response )
            {
                //  Check
                if( $response['resetContexts'] === false )
                {
                    //  Affected Contexts
                    $affectedContexts = array();

                    //  Loop Each
                    foreach( $response['affectedContexts'] as $aContext )
                    {
                        //  Append
                        $affectedContexts[] = $aContext['name'];
                    }

                    //  Read the Contexts
                    $contexts = array_unique( array_merge( $contexts, $affectedContexts ) );
                }
            }
        }

        //  Return
        return $contexts;
    }


    /**
     * @inheritdoc
     */
    public function makeMessages( AIFulfillmentInterface $filler = null, EventInterface $event = null, $ignore_platform = false )
    {
        //  Messages
        $messages = array();

        //  Get Platform
        $platform = mManager()->getPlatform();

        //  Switch
        switch( $platform )
        {
            //  Facebook
            case 'facebook':

                //  Generate Messages
                $messages = ( new Factory\FacebookAPIAIFactory( $filler, $event, $ignore_platform ) )->getMessages();
                break;

            //  Default
            default:

                //  Throw Exception
                throw new \Exception( "Service Factory to generate messages for '{$platform}' is not available" );
                break;
        }

        //  Return
        return $messages;
    }
}