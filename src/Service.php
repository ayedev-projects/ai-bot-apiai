<?php namespace Ayedev\Integration\ApiAI;

use Ayedev\Bot\AI\Core\AI;
use Ayedev\Bot\AI\Core\CommonResponse;
use Ayedev\Bot\Messenger\Core\ApiCall;

class Service extends AI
{
    /**
     * Api.ai API Base URI.
     */
    const API_BASE_URI = 'https://api.api.ai';

    /**
     * Api.ai API Version
     */
    const API_VERSION = '20150910';

    /**
     * Api.ai API Version Endpoint
     */
    const API_VERSION_ENDPOINT = 'v1';

    /**
     * Converse Message API Endpoint.
     */
    const QUERY_MESSAGE_API = '/query';

    /**
     * Read Entities API Endpoint.
     */
    const ENTITY_READ_API = '/entities';

    /**
     * Contexts API Endpoint.
     */
    const CONTEXTS_API = '/contexts';

    /**
     * Intents API Endpoint.
     */
    const INTENTS_API = '/intents';


    /**
     * @inheritdoc
     */
    public function readTextIntent( $q, $params = [] )
    {
        //  Return
        return $this->converseText( null, $q, $params );
    }

    /**
     * @inheritdoc
     */
    public function converseText( $session_id, $q, $params = [] )
    {
        //  Defs
        $query = array( 'v' => static::API_VERSION, 'query' => $q, 'lang' => $this->_lang, 'timezone' => gmdate( 'Y-m-d\TH:i:sO' ) );

        //  Check
        if( $session_id )   $query['sessionId'] = $session_id;

        //  Return
        return new Response( $this->getAPICall()->send( 'GET', static::QUERY_MESSAGE_API, null, array_merge( $query, $params ), $this->getHeaders() )->getResponseDecoded() );
    }

    /**
     * @inheritdoc
     */
    public function triggerEvent( $session_id, $event_name, $event_data = [], $params = [] )
    {
        //  Defs
        $query = array( 'v' => static::API_VERSION, 'lang' => $this->_lang, 'timezone' => date_default_timezone_get(), 'event' => array( 'name' => $event_name , 'data' => $event_data ) );

        //  Check
        if( $session_id )   $query['sessionId'] = $session_id;

        //  Return
        return new Response( $this->getAPICall()->send( 'POST', static::QUERY_MESSAGE_API, array_merge( $query, $params ), array(), $this->getHeaders() )->getResponseDecoded() );
    }

    /**
     * @inheritdoc
     */
    public function getIntent( $intent_id, $params = [] )
    {
        //  Return
        return new CommonResponse( $this->getAPICall()->send( 'GET', static::INTENTS_API . '/' . $intent_id, null, $params, $this->getHeaders( $this->getDeveloperToken() ) )->getResponseDecoded() );
    }

    /**
     * @inheritdoc
     */
    public function getConverseFromEvent( $session_id, $event_name, $event_data = [], $params1 = [], $params2 = [] )
    {
        //  Trigger Event
        $eventResponse = $this->triggerEvent( $session_id, $event_name, $event_data, $params1 );

        //  Check
        if( $eventResponse && $eventResponse->isSuccess() )
        {
            //  Get Real Response
            $realResponse = $eventResponse->getResponse();

            //  Intent ID
            $intentID = $realResponse['result']['metadata']['intentId'];

            //  Get Intent Info
            $intent = $this->getIntent( $intentID, $params2 );

            //  Return
            return Response::makeFromIntent( $intent, $realResponse );
        }

        //  Return
        return null;
    }


    /**
     * @inheritdoc
     */
    public function getContexts( $session_id, $params = [] )
    {
        //  Return
        return new CommonResponse( $this->getAPICall()->send( 'GET', static::CONTEXTS_API, null, array_merge( array( 'sessionId' => $session_id ), $params ), $this->getHeaders() )->getResponseDecoded() );
    }

    /**
     * <@inheritdoc></@inheritdoc>
     */
    public function getContext( $session_id, $context, $params = [] )
    {
        //  Return
        return new CommonResponse( $this->getAPICall()->send( 'GET', static::CONTEXTS_API . '/' . $context, null, array_merge( array( 'sessionId' => $session_id ), $params ), $this->getHeaders() )->getResponseDecoded() );
    }

    /**
     * @inheritdoc
     */
    public function postContext( $session_id, $context_data, $params = [] )
    {
        //  Return
        return new CommonResponse( $this->getAPICall()->send( 'POST', static::CONTEXTS_API, $context_data, array_merge( array( 'sessionId' => $session_id ), $params ), $this->getHeaders() )->getResponseDecoded() );
    }

    /**
     * @inheritdoc
     */
    public function clearContexts( $session_id, $params = [] )
    {
        //  Return
        return new CommonResponse( $this->getAPICall()->send( 'DELETE', static::CONTEXTS_API, null, array_merge( array( 'sessionId' => $session_id ), $params ), $this->getHeaders() )->getResponseDecoded() );
    }

    /**
     * @inheritdoc
     */
    public function deleteContext( $session_id, $context, $params = [] )
    {
        //  Return
        return new CommonResponse( $this->getAPICall()->send( 'DELETE', static::CONTEXTS_API . '/' . $context, null, array_merge( array( 'sessionId' => $session_id ), $params ), $this->getHeaders() )->getResponseDecoded() );
    }

    /**
     * @inheritdoc
     */
    public function deleteContexts( $session_id, $contexts, $params = [] )
    {
        //  Responses
        $responses = array();

        //  Loop Each
        foreach( (array)$contexts as $context )
        {
            //  Append
            $responses[$context] = $this->deleteContext( $session_id, $context, $params );
        }

        //  Return
        return $responses;
    }

    /**
     * @inheritdoc
     */
    public function onlyContexts( $session_id, $contexts, $params = [], $params2 = [] )
    {
        //  Responses
        $responses = array();

        //  Get Current Contexts
        $currentContexts = $this->getContexts( $session_id, $params2 );

        //  Loop Each
        foreach( $currentContexts->getResponse() as $contextData )
        {
            //  Check
            if( !in_array( $contextData['name'], (array)$contexts ) )
            {
                //  Delete Context
                $responses[$contextData['name']] = $this->deleteContext( $session_id, $contextData['name'], $params );
            }
        }

        //  Return
        return $responses;
    }

    /**
     * @inheritdoc
     */
    public function assignContext( $session_id, $context, $parameters = [], $lifespan = 10, $params = [] )
    {
        //  Return
        return new CommonResponse( $this->getAPICall()->send( 'POST', static::CONTEXTS_API, array( 'name' => $context, 'lifespan' => $lifespan, 'parameters' => $parameters ), array_merge( array( 'sessionId' => $session_id ), $params ), $this->getHeaders() )->getResponseDecoded() );
    }

    /**
     * Extract Static Messages
     *
     * @return array
     */
    protected function extractStaticMessages()
    {
        //  Messages
        $messages = array();

        //  Check
        if( $this->_developerToken && $this->_staticMessageEntityKey )
        {
            //  Response
            $response = $this->getAPICall()->send( 'GET', static::ENTITY_READ_API . '/' . $this->_staticMessageEntityKey, null, [], $this->getHeaders( $this->_developerToken ) )->getResponseDecoded();

            //  Check
            if( $response && isset( $response['entries'] ) )
            {
                //  Loop Each
                foreach( $response['entries'] as $entry )
                {
                    //  Store
                    $messages[$entry['value']] = $entry['synonyms'][ mt_rand( 0, sizeof( $entry['synonyms'] ) - 1 ) ];
                    $messages[$entry['value'] . ':all'] = $entry['synonyms'];
                }
            }
        }

        //  Return
        return $messages;
    }

    /**
     * @inheritdoc
     */
    public function readIntentMappings( $force = false, $params = array() )
    {
        //  Mappings Data
        $mappings = array();

        //  Cache Key
        $cacheKey = 'intents_' . md5( $this->getDeveloperToken() );

        //  Cache File
        $cacheFile = $this->cacheFile( $cacheKey );

        //  Check
        if( file_exists( $cacheFile ) )
        {
            //  Mappings
            $mappings = @json_decode( @file_get_contents( $cacheFile ), true );
        }

        //  Check
        if( $force || !$mappings )
        {
            //  Get All Intents
            $intentsResponse = new Response( $this->getAPICall()->send( 'GET', static::INTENTS_API, null, $params, $this->getHeaders( $this->getDeveloperToken() ) )->getResponseDecoded() );

            //  Clear
            $mappings = array();

            //  Get Result
            $result = $intentsResponse->getResponse();

            //  Loop Each
            foreach( $result as $intentDef )
            {
                //  Check
                if( $intentDef['state'] == 'LOADED' )
                {
                    //  Contexts & Parameters
                    $contexts = $parameters = array();

                    //  Loop Each Parameters
                    foreach( $intentDef['parameters'] as $paramDef )
                    {
                        //  Append
                        $parameters[] = $paramDef['name'];
                    }

                    //  Loop Each Contexts
                    foreach( $intentDef['contextOut'] as $contextDef )
                    {
                        //  Append
                        $contexts[] = $contextDef['name'];
                    }

                    //  Append
                    $mappings[$intentDef['id']] = array(
                        'id' => $intentDef['id'],
                        'name' => $intentDef['name'],
                        'priority' => $intentDef['priority'],
                        'action' => ( isset( $intentDef['actions'] ) && sizeof( $intentDef['actions'] ) > 0 ? $intentDef['actions'][0] : null ),
                        'is_fallback' => $intentDef['fallbackIntent'],
                        'contexts' => $contexts,
                        'parameters' => $parameters
                    );
                }
            }

            //  Save to Cache File
            file_put_contents( $cacheFile, json_encode( $mappings ) );
        }

        //  Return
        return $mappings;
    }

    /**
     * @inheritdoc
     */
    public function getValidIntentContexts( $intent_id )
    {
        //  Contexts
        $contexts = array();

        //  Get Intent Data
        $intentData = $this->readIntentMappings();

        //  Check
        if( $intentData && isset( $intentData[$intent_id] ) )
        {
            //  Read
            $contexts = $intentData[$intent_id]['contexts'];
        }

        //  Return
        return $contexts;
    }
}